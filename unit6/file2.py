import file1


class BirthdayCard(file1.GreetingCard):

    def __init__(self, sender_age=0):
        file1.GreetingCard.__init__(self)
        self._sender_age = sender_age

    def greeting_msg(self):
        print(f"Sender: {self._sender}")
        print(f"Recipient: {self._recipient}")
        print(f"Sender age: {self._sender_age}")
