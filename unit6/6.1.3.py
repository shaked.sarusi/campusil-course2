from tkinter import *
import tkinter as tk

# create window
root = Tk()
root.geometry("1000x800")

# create label with question
my_font = ('Helvetica', 24, 'bold')
question_label = Label(root, text="What's my favorite animal?", font=my_font)
question_label.pack()


# create button
def show_image():
    # open and display image
    image_file = 'dogPhoto.png'
    image = tk.PhotoImage(file=image_file)
    label = tk.Label(root, image=image)
    label.pack()
    label.image = image  # keep a reference to the PhotoImage object


button = Button(root, text="Click me and find out!", command=show_image)
button.pack()

# run main loop
root.mainloop()
