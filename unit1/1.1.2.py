import functools

def repeat_char(char):
    return char*2

def double_letter(my_str):
    return ''.join(map(repeat_char, my_str))



print(double_letter("python"))
print(double_letter("we are the champions!"))