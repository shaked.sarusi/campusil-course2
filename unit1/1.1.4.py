import functools


def calculate(x, y):
    return int(x) + int(y)

def sum_of_digits(number):
        return functools.reduce(calculate, str(number))


print(sum_of_digits(310))