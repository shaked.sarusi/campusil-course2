#task 1
longest_name = max(open("names.txt"), key=len).strip()
print(longest_name)

#task 2
print(sum(len(line.strip()) for line in open("names.txt")))

#task 3
shortest_names = [line.strip() for line in open("names.txt") if len(line.strip()) == min(len(line.strip()) for line in open("names.txt"))]
print('\n'.join(shortest_names))

#task 4
with open("name_length.txt", "w") as f:
    f.write('\n'.join(str(len(line.strip())) for line in open("names.txt")))

#task 5
length = int(input("Enter name length: "))
names = [line.strip() for line in open("names.txt") if len(line.strip()) == length]
print('\n'.join(names))

