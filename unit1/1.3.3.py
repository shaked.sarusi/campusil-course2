def is_funny(string):
    return list(set([False if char != 'h' and char != 'a' else True for char in string]))[0]

print(is_funny("hahaahahha"))
