class BigThing:

    def __init__(self, param):
        self._param = param

    def size(self):
        if isinstance(self._param, int):
            return self._param

        if isinstance(self._param, dict):
            return len(self._param)
        if isinstance(self._param, str):
            return len(self._param)
        if isinstance(self._param, list):
            return len(self._param)


class BigCat(BigThing):

    def __init__(self, param, weight):
        BigThing.__init__(self, param)
        self._weight = weight

    def size(self):
        if(self._weight > 15):
            if (self._weight>20):
                return "Very Fat"
            return "Fat"
        return "OK"

cutie = BigCat("mitzy", 22) 
print(cutie.size())
