class Animal:
    zoo_name = "Hayaton"

    def __init__(self, name, hunger=0):
        self._name = name
        self._hunger = hunger

    def get_name(self):
        return self._name

    def is_hungry(self):
        return self._hunger > 0

    def get_hungry(self):
        return self._hunger

    def feed(self):
        if self._hunger > 0:
            self._hunger -= 1

    def talk(self):
        pass


class Dog(Animal):
    def fetch_stick(self):
        print("There you go, sir!")

    def talk(self):
        print("woof woof")


class Cat(Animal):
    def chase_laser(self):
        print("Meeeeow")

    def talk(self):
        print("Meow")


class Skunk(Animal):
    def __init__(self, name, hungar=0):
        super().__init__(name, hungar)
        self._stink_count = 6

    def stink(self):
        print("Dear Lord!")

    def talk(self):
        print("tsssss")


class Unicorn(Animal):
    def sing(self):
        print("I'm not your toy...")

    def talk(self):
        print("Good day, darling")


class Dragon(Animal):
    def __init__(self, name, hungar=0):
        super().__init__(name, hungar)
        self._color = "Green"

    def breath_fire(self):
        print("$@#$#@$")

    def talk(self):
        print("Raaaawr")


if __name__ == '__main__':
    brownie = Dog("Brownie", 10)
    zelda = Cat("Zelda", 3)
    stinky = Skunk("Stinky", 0)
    keith = Unicorn("Keith", 7)
    lizzy = Dragon("Lizzy", 1450)

    zoo_lst = [brownie, zelda, stinky, keith, lizzy]

    Doggo = Dog("Doggo", 80)
    Kitty = Cat("Kitty", 80)
    StinkyJr = Skunk("Stinky Jr.", 80)
    Clair = Unicorn("Clair", 80)
    McFly = Dragon("McFly", 80)

    zoo_lst.append(Doggo)
    zoo_lst.append(Kitty)
    zoo_lst.append(StinkyJr)
    zoo_lst.append(Clair)
    zoo_lst.append(McFly)

    for animal in zoo_lst:
        while animal.is_hungry():
            animal.feed()

        print(animal.__class__.__name__, animal.get_name())
        animal.talk()

        if isinstance(animal, Dog):
            animal.fetch_stick()
        elif isinstance(animal, Cat):
            animal.chase_laser()
        elif isinstance(animal, Skunk):
            animal.stink()
        elif isinstance(animal, Unicorn):
            animal.sing()
        elif isinstance(animal, Dragon):
            animal.breath_fire()

    print(Kitty.zoo_name)
