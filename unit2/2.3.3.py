class Dog:
    count_animals = 0

    def __init__(self, age, name="Octavio"):
        self._name = name
        self._age = age
        Dog.count_animals = Dog.count_animals + 1

    def birthday(self):
        self._age += 1

    def get_age(self):
        return self._age

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name


rex = Dog(3)
bolt = Dog(5, "bolt")


print(rex.get_name())
print(bolt.get_name())

rex.set_name("rex")
print(rex.get_name())

print(bolt.count_animals)
