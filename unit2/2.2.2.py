class Dog:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def birthday(self):
        self.age += 1

    def get_age(self):
        return self.age


rex = Dog("rex", 3)
bolt = Dog("bolt", 5)

rex.birthday()

print(rex.get_age())
print(bolt.get_age())