class Pixel:

    def __init__(self, x=0, y=0, red=0, green=0, blue=0):
        self._x = x
        self._y = y
        self._red = red
        self._green = green
        self._blue = blue

    def set_coords(self, x, y):
        self._x = x
        self._y = y

    def set_grayscale(self):
        mean = int((self._red + self._blue + self._green) / 3)
        self._red = mean
        self._green = mean
        self._blue = mean

    def print_pixel_info(self):
        color = ""
        if (self._red == 0 and self._green == 0):
            color = "Blue"
        if (self._red == 0 and self._blue == 0):
            color = "Green"
        if (self._blue == 0 and self._green == 0):
            color = "Red"
        if (self._blue == 0 and self._green == 0 and self._red == 0):
            color = ""

        print(
            "X: {}, Y: {}, Color: ({}, {}, {}) {}".format(self._x, self._y, self._red, self._green, self._blue, color))


my_pixel = Pixel(5, 6, 250, 0, 0)
my_pixel.print_pixel_info()
my_pixel.set_grayscale()
my_pixel.print_pixel_info()