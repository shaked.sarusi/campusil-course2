def gen_secs():
    for i in range(60):
        yield i


def gen_minutes():
    for i in range(60):
        yield i


def gen_hours():
    for i in range(24):
        yield i


def get_time():
    for hour in gen_hours():
        for minute in gen_minutes():
            for sec in gen_secs():
                yield f"{hour:02d}:{minute:02d}:{sec:02d}"


def gen_years(start=2019):
    for year in range(start, 2020):
        yield year


def gen_months():
    for month in range(1, 13):
        yield month


def gen_days(month, leap_year=True):
    days_in_month = {
        1: 31,
        2: 29 if leap_year else 28,
        3: 31,
        4: 30,
        5: 31,
        6: 30,
        7: 31,
        8: 31,
        9: 30,
        10: 31,
        11: 30,
        12: 31
    }
    yield days_in_month[month]


def is_leap(year):
    if (year % 400 == 0) and (year % 100 == 0):
        return True

    elif (year % 4 == 0) and (year % 100 != 0):
        return True

    else:
        return False


def gen_date():
    for year in gen_years(2010):
        leap_year = is_leap(year)
        for month in gen_months():
            for day in gen_days(month, leap_year):
                for hour in gen_hours():
                    for minute in gen_minutes():
                        for second in gen_secs():
                            yield f"{day:02d}/{month:02d}/{year} {hour:02d}:{minute:02d}:{second:02d}"


gen = gen_date()
i = 0
while True:
    date = next(gen)
    if i % 1000000 == 0:
        print(date)
    i += 1
