def parse_ranges(ranges_string):
    list_of_ranges = (ranges_string.split(","))
    ranges_list = (list(range.split("-")) for range in list_of_ranges)
    ranges = (i for x in ranges_list for i in range(int(x[0]), int(x[1])+1))
    return ranges


print(list(parse_ranges("1-2,4-4,8-10")))
print(list(parse_ranges("0-0,4-8,20-21,43-45")))

