import sys


def get_fibo():
    x1 = 0
    x2 = 1
    yield x1
    yield x2
    while True:
        x3 = x1+x2
        yield x3
        x1 = x2
        x2 = x3


fibo_gen = get_fibo()
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))