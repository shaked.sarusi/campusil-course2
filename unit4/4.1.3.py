def first_prime_over(n):
    my_generator = (i for i in range(n + 1, 2 * n) if i > 1)
    for num in my_generator:
        if is_prime(num):
            return num


def is_prime(n):
    # Corner case
    if n <= 1:
        return False
    # Check from 2 to n-1
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


print(first_prime_over(1000000))
