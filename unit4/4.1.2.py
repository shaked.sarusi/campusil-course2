def translate(sentence):
    words = {'esta': 'is', 'la': 'the', 'en': 'in', 'gato': 'cat', 'casa': 'house', 'el': 'the'}
    sentence_split = sentence.split(" ")
    my_generator = (words[key] for sp_word in sentence_split for key in words if key == sp_word)
    for generator in my_generator:
        print(generator)


translate("el gato esta en la casa")