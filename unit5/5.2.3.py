import itertools

bills = [20] * 3 + [10] * 5 + [5] * 2 + [1] * 5
combinations = set()

for r in range(1, len(bills) + 1):
    for combination in itertools.combinations(bills, r):
        if sum(combination) == 100:
            combinations.add(tuple(sorted(combination)))

for combination in combinations:
    print(combination)

print(f"Number of options: {len(combinations)}")
