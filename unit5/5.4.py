import random


class IDIterator:
    _id = 0

    def __init__(self, id):
        self._id = id
        self._count = 0

    def __iter__(self):
        return self

    def __next__(self):

        while self._count < 10:
            self._id += 1
            if check_id_valid(self._id):
                self._count += 1
                return self._id

        raise StopIteration


def id_generator(id_number):
    count = 0
    while id_number < 1000000000 and count < 10:
        id_number += 1
        if check_id_valid(id_number):
            count += 1
            yield id_number


def check_id_valid(id_number):
    list_of_num = list(map(int, str(id_number)))

    if len(list_of_num) != 9:
        return False

    for i in range(9):
        if i % 2 == 1:
            list_of_num[i] *= 2

    for i in range(9):
        if list_of_num[i] > 10:
            list_of_num[i] = int(list_of_num[i] / 10) + list_of_num[i] % 10

    Sum = sum(list_of_num)
    if Sum % 10 == 0:
        return True
    else:
        return False


def main():

    ID = int(input("Enter ID: "))
    gen_or_iter = input("Generator or Iterator? (gen/it)? ")
    if gen_or_iter == "gen":
        for id_current in id_generator(ID):
            print(id_current)
    else:
        id_iter = iter(IDIterator(ID))
        for x in id_iter:
            print(x)

if __name__ == "__main__":
    main()