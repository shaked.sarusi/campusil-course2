class MusicNotes:
    def __init__(self):
        self.notes = [
            (55, 110, 220, 440, 880),
            (61.74, 123.48, 246.96, 493.92, 987.84),
            (65.41, 130.82, 261.64, 523.28, 1046.56),
            (73.42, 146.84, 293.68, 587.36, 1174.72),
            (82.41, 164.82, 329.64, 659.28, 1318.56),
            (87.31, 174.62, 349.24, 698.48, 1396.96),
            (98, 196, 392, 784, 1568)
        ]
        self._note = 0
        self._octave = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self._note == len(self.notes):
            raise StopIteration

        freq = self.notes[self._note][self._octave]
        self._octave += 1

        if self._octave == len(self.notes[self._note]):
            self._note += 1
            self._octave = 0
        return freq


notes_iter = iter(MusicNotes())
for freq in notes_iter:
    print(freq)
