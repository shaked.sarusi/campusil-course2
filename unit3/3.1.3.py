def throw_StopIteration():
    lst = [1, 2, 3]
    iterator = iter(lst)
    next(iterator)
    next(iterator)
    next(iterator)
    next(iterator)


def throw_ZeroDivisionError():
    x = 1 / 0


def throw_AssertionError():
    assert False, "This assertion is always false!"


def throw_ImportError():
    import some_non_existing_module


def throw_KeyError():
    d = {"key1": "value1", "key2": "value2"}
    value = d["key3"]


def throw_SyntaxError():
    eval("3 3")


def throw_IndentationError():
    def some_function():
        print("This function will not run because of the indentation error")
        print("This line is indented too much")


def throw_TypeError():
    x = "1" + 2

#throw_StopIteration()
#throw_IndentationError()
#throw_KeyError()
#throw_SyntaxError()
#throw_ImportError()
#throw_AssertionError()
#throw_TypeError()
#throw_ZeroDivisionError()