import string

class UsernameContainsIllegalCharacter(Exception):
    def __init__(self, illegal_char, index):
        self.illegal_char = illegal_char
        self.index = index

    def __str__(self):
        return f"The username contains an illegal character '{self.illegal_char}' at index {self.index}"


class UsernameTooShort(Exception):
    def __str__(self):
        return "The username is too short"

class UsernameTooLong(Exception):
    def __str__(self):
        return "The username is too long"

class PasswordMissingCharacter(Exception):
    def __str__(self):
        return "The password is missing a mandatory character"


class PasswordMissingUppercase(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Uppercase)"


class PasswordMissingLowercase(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Lowercase)"


class PasswordMissingDigit(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Digit)"


class PasswordMissingSpecial(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Special)"

class PasswordTooShort(Exception):
    def __str__(self):
        return "The password is too short"

class PasswordTooLong(Exception):
    def __str__(self):
        return "The password is too long"

def check_input(username, password):
    # Check username
    i=0
    for char in username:

        if not (char.isalnum() or char == '_'):
            raise UsernameContainsIllegalCharacter(char, i)
        i += 1

    if len(username) < 3:
        raise UsernameTooShort()

    if len(username) > 16:
        raise UsernameTooLong()


    # Check password
    countUpper = 0
    countLower = 0
    countDigit = 0
    countPunctuation = 0
    for char in password:
        if char.isupper():
            countUpper+=1

        if char.islower():
            countLower+=1

        if char.islower():
            countDigit+=1

        if char in string.punctuation:
            countPunctuation+=1

    if countDigit == 0:
        raise PasswordMissingDigit()
    if countLower == 0:
        raise PasswordMissingLowercase()
    if countUpper == 0:
        raise PasswordMissingUppercase()
    if countPunctuation == 0:
        raise PasswordMissingSpecial()


    if len(password) < 8:
            raise PasswordTooShort()
    if len(password) > 40:
            raise PasswordTooLong()


    # If both username and password are valid, return True
    return True

def main():
    while True:
        try:
            username = input("Enter a username: ")
            password = input("Enter a password: ")
            check_input(username, password)
            print("OK")
            break
        except UsernameTooShort as e:
            print(e)
        except UsernameTooLong as e:
            print(e)
        except UsernameContainsIllegalCharacter as e:
            print(e)
        except PasswordTooShort as e:
            print(e)
        except PasswordTooLong as e:
            print(e)
        except PasswordMissingCharacter as e:
            print(e)

if __name__ == '__main__':
    main()
