class UnderAge(Exception):
    def __str__(self):
        return f"you're under 18 years old. You're {self.args[0]} years old, so in a {18-self.args[0]} year you'll be able to come to Ido's birthday!"


def send_invitation(name, age):
    if int(age) < 18:
        raise UnderAge(age)
    else:
        print("You should send an invite to " + name)


try:
    send_invitation("John", 17)
except UnderAge as e:
    print(e)

try:
    send_invitation("Jane", 20)
except UnderAge as e:
    print(e)