def read_file(file_name):
    print("__CONTENT_START__")
    try:
        with open(file_name, "r") as f:
            line = f.read()
    except FileNotFoundError:
        print("__NO_SUCH_FILE__")

    else:
        print("__CONTENT_START__")
        print(line)
    finally:
        print("__CONTENT_END__")



read_file("one_lined_file.txt")
read_file("file_does_not_exist.txt")